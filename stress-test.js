import http from 'k6/http';
import {sleep} from 'k6';
import { check, group } from "k6";
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export const options = {
  stages: [
    { duration: '10m', target: 200 }, 
    { duration: '30m', target: 200 }, 
    { duration: '5m', target: 0 }, 
  ],
};

export default () => {
  const urlRes = http.get("https://run.mocky.io/v3/1b6d9480-44c4-49ae-a095-27e780edf0eb");
  sleep(1);
};

export function handleSummary(data) {
  return {
    "summary.html": htmlReport(data),
  };
}